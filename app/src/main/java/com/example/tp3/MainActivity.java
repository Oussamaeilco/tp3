package com.example.tp3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        List<JeuVideo> mesJeux=new ArrayList<>();
        mesJeux.add(new JeuVideo("Hollow Knight",15));
        mesJeux.add(new JeuVideo("Skyrim SE",45));
        mesJeux.add(new JeuVideo("Fallout 4 Ultimate Edition",45));
        mesJeux.add(new JeuVideo("Fallout NV",23.5));
        mesJeux.add(new JeuVideo("Elder Scrolls:Morrowind",10));
        mesJeux.add(new JeuVideo("Hollow Knight 2",25));
        mesJeux.add(new JeuVideo("Biohazard 7",35));

        mesJeux.add(new JeuVideo("Hollow Knight",15));
        mesJeux.add(new JeuVideo("Skyrim SE",45));
        mesJeux.add(new JeuVideo("Fallout 4 Ultimate Edition",45));
        mesJeux.add(new JeuVideo("Fallout NV",23.5));
        mesJeux.add(new JeuVideo("Elder Scrolls:Morrowind",10));
        mesJeux.add(new JeuVideo("Hollow Knight 2",25));
        mesJeux.add(new JeuVideo("Biohazard 7",35));

        mesJeux.add(new JeuVideo("Hollow Knight",15));
        mesJeux.add(new JeuVideo("Skyrim SE",45));
        mesJeux.add(new JeuVideo("Fallout 4 Ultimate Edition",45));
        mesJeux.add(new JeuVideo("Fallout NV",23.5));
        mesJeux.add(new JeuVideo("Elder Scrolls:Morrowind",10));
        mesJeux.add(new JeuVideo("Hollow Knight 2",25));
        mesJeux.add(new JeuVideo("Biohazard 7",35));

        mesJeux.add(new JeuVideo("Hollow Knight",15));
        mesJeux.add(new JeuVideo("Skyrim SE",45));
        mesJeux.add(new JeuVideo("Fallout 4 Ultimate Edition",45));
        mesJeux.add(new JeuVideo("Fallout NV",23.5));
        mesJeux.add(new JeuVideo("Elder Scrolls:Morrowind",10));
        mesJeux.add(new JeuVideo("Hollow Knight 2",25));
        mesJeux.add(new JeuVideo("Biohazard 7",35));

        mesJeux.add(new JeuVideo("Hollow Knight",15));
        mesJeux.add(new JeuVideo("Skyrim SE",45));
        mesJeux.add(new JeuVideo("Fallout 4 Ultimate Edition",45));
        mesJeux.add(new JeuVideo("Fallout NV",23.5));
        mesJeux.add(new JeuVideo("Elder Scrolls:Morrowind",10));
        mesJeux.add(new JeuVideo("Hollow Knight 2",25));
        mesJeux.add(new JeuVideo("Biohazard 7",35));

        mesJeux.add(new JeuVideo("Hollow Knight",15));
        mesJeux.add(new JeuVideo("Skyrim SE",45));
        mesJeux.add(new JeuVideo("Fallout 4 Ultimate Edition",45));
        mesJeux.add(new JeuVideo("Fallout NV",23.5));
        mesJeux.add(new JeuVideo("Elder Scrolls:Morrowind",10));
        mesJeux.add(new JeuVideo("Hollow Knight 2",25));
        mesJeux.add(new JeuVideo("Biohazard 7",35));

        RecyclerView myRecyclerView = findViewById(R.id.myRecyclerView);
        myRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        myRecyclerView.setAdapter( new JeuxVideoAdapter(mesJeux));
    }
}
